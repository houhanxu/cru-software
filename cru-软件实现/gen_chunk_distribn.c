// this function is to generate the chunk distribution and keep it 
// the information include: 
//                         a. the mapping information of chunk_id to node_id

#define _GNU_SOURCE 


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <time.h>
#include <malloc.h>

#include "config2.h"
#include "common2.h"


//this function is to get the chunk distribution of each chunk 
//we assume that each rack is composed of a constant number of nodes 
//the number of chunks in a rack should not exceed max_chunks_per_rack
void init_parix_fo_gen_chunk_distrbtn(){

    int i,j;
    //int base;
    //int rank;
    int node_id;
    int rack_id;

    int* block_to_node=(int*)malloc(sizeof(int)*num_blocks_in_stripe);

    int* block_map=(int*)malloc(sizeof(int)*stripe_num*num_blocks_in_stripe); // maps chunk_id to node_id

    //int* flag_index=(int*)malloc(sizeof(int)*total_nodes_num);
    int* num_block_in_rack=(int*)malloc(sizeof(int)*rack_num);


    srand((unsigned int)time(0));

    for(i=0; i<stripe_num; i++){

        memset(block_to_node, -1, sizeof(int)*num_blocks_in_stripe);
        memset(num_block_in_rack, 0, sizeof(int)*rack_num);

        //generate the distribution of each stripe randomly

        //for(j=0; j<total_nodes_num; j++)
        //    flag_index[j]=j;

        // base=total_nodes_num;

        for(j=0; j<num_blocks_in_stripe; j++){

            //rank=j/p;
            //node_id=flag_index[rank];
			node_id = j / p;
            rack_id=get_rack_id(node_id);

            if(num_block_in_rack[rack_id]>max_block_per_rack){
                /*j--;
                continue;*/
				printf("over!over!");
            }

            block_to_node[j]=node_id;
            //flag_index[rank]=flag_index[total_nodes_num-j-1];
            num_block_in_rack[rack_id]++;

            //base--;

        }

        //printf("%d-th stripe node_map:\n",i);
        for(j=0; j<num_blocks_in_stripe; j++)
            block_map[i*num_blocks_in_stripe+j]=block_to_node[j];

    }

    //write the mapping info to a chunk_2_node 
    FILE *fd; 

    fd=fopen("block_map","w");
    for(i=0; i<stripe_num; i++){

        for(j=0; j<num_blocks_in_stripe; j++)
            fprintf(fd, "%d ", block_map[i*num_blocks_in_stripe+j]);

        fprintf(fd, "\n");
    }

    fclose(fd);

    free(block_map);
    free(block_to_node);
    //free(flag_index);
    free(num_block_in_rack);


}


int main(int argc, char** argv){

    init_parix_fo_gen_chunk_distrbtn();

    return 0;

}

