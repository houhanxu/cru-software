#define _GNU_SOURCE2

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <pthread.h>
#include <net/if.h>
#include <netinet/in.h>
#include <net/if_arp.h>
#include <arpa/inet.h>

#include "common2.h"
#include "config2.h"

#define UPPBND 9999

int num_rcrd_strp;



/**
 * A thread to send a command to a node
*/
void* send_cmd_process(void* ptr){

    CMD_DATA tcd = *(CMD_DATA *)ptr;

    tcd.port_num=UPDT_PORT;
    
    // if the system is on a local cluster with a node served as gateway, then send the data to the gateway first
    // otherwise, send the data to the destination node directly
    if(GTWY_OPEN)
        send_data(NULL, gateway_ip, UPDT_PORT, NULL, (CMD_DATA*)ptr, CMD_INFO);

    else
        send_data(NULL, tcd.next_ip, UPDT_PORT, NULL, (CMD_DATA*)ptr, CMD_INFO);    
    
    return NULL;
    
}


/* This function establishes the map of a data block to a external node(outside the rack).
 * This external node is uesd for interim log replica of the delta , so as to
 * promise any single node/rack failure
 * we put the delta to one of the nodes which contain the needed update parity block
 * while the same data block update, we also put the repli to the same outside node.
*/
void cru_estbh_log_map(){
    
    int i,j,k;
    int row,col;
    int its_node_id;
    int its_rack_id;
    int prty_nd_id;
    int prty_rack_id;

    memset(prty_log_map, -1, sizeof(int)*stripe_num*data_blocks);
	int* prty_block_id = (int*)malloc(sizeof(int)*r);

    for(i=0; i<stripe_num; i++){
        for(j=0; j<data_blocks; j++){

                row=j%(p-2);
                col=j/(p-2);
				
                its_node_id=global_block_map[i*num_blocks_in_stripe+j/(p-2)*p+j%(p-2)];
                its_rack_id=get_rack_id(its_node_id);
                get_parity_blcks(row, col, prty_block_id);
				

                for(k=0; k<r; k++){
                    prty_nd_id=global_block_map[prty_block_id[k]];
                    prty_rack_id=get_rack_id(prty_nd_id);
				
                    if(prty_rack_id!=its_rack_id && prty_nd_id != 0){
                        prty_log_map[i*data_blocks+j]=prty_nd_id;
                        break;
                    }
                }
                
				
            
        }
    }
	free(prty_block_id);
    // write the mapping info into a file
    FILE *fd;
    char* filename="cru_parity_log_map";

    fd=fopen(filename, "w");
    if(fd==NULL)
        printf("openfile error!\n");

    for(i=0; i<stripe_num; i++){
        for(j=0; j<data_blocks; j++)
            fprintf(fd, "%d ", prty_log_map[i*data_blocks+j]);
        
		fprintf(fd, "\n");
    }

    fclose(fd);

}

/**
 * This is a function for parity update. In this function, the metadata server will
 * decide the roles of each involved node(including the parity nodes, and the data nodes)
*/
void cru_prty_updt(int num_rcrd_strp){

    printf("******Parity update starts:******\n");

    int i,j,k,h;
    int prty_node_id;
    int parity_block_id;
    int row,col;
    int prty_rack_id;
    int updt_stripe_id;
    int count1;
	int count3;
    int node_id, rack_id;
    //int updt_block_num;
    int dt_global_block_id;
	int global_block_id;
    int copy_node_id;
    int copy_rack_id;

    CMD_DATA* tcd_prty=(CMD_DATA*)malloc(sizeof(CMD_DATA)*(r));
    CMD_DATA* tcd_dt=(CMD_DATA*)malloc(sizeof(CMD_DATA)*data_blocks);
    CMD_DATA* tcd_copy=(CMD_DATA*)malloc(sizeof(CMD_DATA)*data_blocks);
	int* prty_block_id = (int*)malloc(sizeof(int)*r);

    //// record the number of updated data blocks in each rack
    //int* updt_blck_num_racks=(int*)malloc(sizeof(int)*rack_num);

    pthread_t send_cmd_dt_thread[data_blocks];
    pthread_t send_cmd_copy_thread[data_blocks];
    pthread_t send_cmd_prty_thread[r];          

    // performs parity update for each updated stripe(i.e., the stripe has data blocks updated)
    for(i=0; i<num_rcrd_strp; i++){

        updt_stripe_id=mark_updt_stripes_tab[i*(data_blocks+1)];

        printf("Stripe-%d Update:\n", updt_stripe_id);
		
		
        /*****************************************************/

        // for each data node, we should define their update approaches and roles when committing the deltas to different parity nodes
        count1=0;
		
		for (j = 0; j < data_blocks; j++) {

			if (mark_updt_stripes_tab[i*(data_blocks + 1) + j + 1] == -1)
				continue;

			//memset(prty_block_id, -1, sizeof(int)*r);

			// located the node_id and rack_id
			dt_global_block_id = updt_stripe_id * num_blocks_in_stripe + j / (p - 2)*p + j % (p - 2);
			// get all r parity blocks which need update
			row = j % (p - 2);
			col = j / (p - 2);
			
			get_parity_blcks(row, col, prty_block_id);


			node_id = global_block_map[dt_global_block_id];
			rack_id = get_rack_id(node_id);

			if (node_id == 0)
				continue;

			copy_node_id = prty_log_map[j];
			copy_rack_id = get_rack_id(copy_node_id);	

			/** inform the parity nodes first to let them be ready for commit*/
			memset(send_cmd_prty_thread, 0, sizeof(send_cmd_prty_thread));
			count3 = 0;
			for (h = 0; h < r; h++) {				
					
				global_block_id = updt_stripe_id * num_blocks_in_stripe + prty_block_id[h];
				prty_node_id = global_block_map[prty_block_id[h]];
				prty_rack_id=get_rack_id(prty_node_id);
				if ((prty_node_id == copy_node_id) || (prty_node_id == 0))
					continue;
				// initialize structure
				tcd_prty[count3].send_size = sizeof(CMD_DATA);
				tcd_prty[count3].op_type = DATA_COMMIT;
				tcd_prty[count3].prty_delta_app_role = PARITY;
				tcd_prty[count3].stripe_id = updt_stripe_id;
				tcd_prty[count3].updt_prty_id = prty_node_id;	 
				tcd_prty[count3].port_num = UPDT_PORT;
				tcd_prty[count3].data_block_id = prty_block_id[h];  
				tcd_prty[count3].num_recv_blks_itn = 0;
				//tcd_prty[count3].num_recv_blks_prt = 0;
				tcd_prty[count3].data_delta_app_prty_role = -1;
				strcpy(tcd_prty[count3].from_ip, "");

				// for the nodes, it can read its ip from sent_ip
				memcpy(tcd_prty[count3].sent_ip, node_ip_set[prty_node_id], ip_len);
				memcpy(tcd_prty[count3].next_ip, tcd_prty[count3].sent_ip, ip_len);

				//printf("tcd_prty.sent_ip=%s\n", tcd_prty[h].sent_ip);

				tcd_prty[count3].num_recv_blks_prt = 1;
				tcd_prty[count3].block_store_index = locate_store_index(prty_node_id, global_block_id);

				tcd_prty[count3].data_delta_app_prty_role = PRTY_LEAF;
				count3++;
			}

			

			/*******************************************/
			//// get the number of update blocks in rack_id
			//updt_block_num=updt_blck_num_racks[rack_id];

			//if(updt_block_num==0){
			//    printf("ERR: updt_block_num==0!\n");
			//    exit(1);
			//}

			/** send to data node*/
			// initialize the common configurations to data node
			tcd_dt[j].send_size = sizeof(CMD_DATA);
			tcd_dt[j].op_type = DATA_COMMIT;
			tcd_dt[j].stripe_id = updt_stripe_id;
			tcd_dt[j].data_block_id = j;
			tcd_dt[j].port_num = UPDT_PORT;
			tcd_dt[j].updt_prty_id = -1;
			tcd_dt[j].block_store_index = locate_store_index(node_id, dt_global_block_id);
			tcd_dt[j].num_recv_blks_itn = 0;
			tcd_dt[j].num_recv_blks_prt = 0;
			tcd_dt[j].data_delta_app_prty_role = -1;
			tcd_dt[j].prty_delta_app_role = -1;
			tcd_dt[j].log_type = INSIDE_LOG;
			strcpy(tcd_dt[j].from_ip, "");
			strcpy(tcd_dt[j].next_ip, "");

			memcpy(tcd_dt[j].next_ip, node_ip_set[node_id], ip_len);
			//printf("tcd_dt[j].next_ip=%s\n", tcd_dt[j].next_ip);

			/** send to copy node*/
			// initialize the common configurations to copy node
			tcd_copy[j].send_size = sizeof(CMD_DATA);
			tcd_copy[j].op_type = DATA_COMMIT;
			tcd_copy[j].stripe_id = updt_stripe_id;
			tcd_copy[j].data_block_id = j;
			tcd_copy[j].port_num = UPDT_PORT;
			tcd_copy[j].updt_prty_id = -1;
			tcd_copy[j].block_store_index = locate_store_index(node_id, dt_global_block_id);
			tcd_copy[j].num_recv_blks_itn = 0;
			tcd_copy[j].num_recv_blks_prt = 0;
			tcd_copy[j].data_delta_app_prty_role = -1;
			tcd_copy[j].prty_delta_app_role = -1;
			tcd_copy[j].log_type = OUTSIDE_LOG;
			strcpy(tcd_copy[j].from_ip, "");
			strcpy(tcd_copy[j].next_ip, "");

			memcpy(tcd_copy[j].next_ip, node_ip_set[copy_node_id], ip_len);
			//printf("tcd_copy[j].next_ip=%s\n", tcd_copy[j].next_ip);

			// for inside_log it only send the delta to node within the same rack
			for (k = 0; k < r; k++) {

				tcd_dt[j].commit_app[k] = 0;
				tcd_copy[j].commit_app[k] = 0;

				parity_block_id = prty_block_id[k];
				prty_node_id = global_block_map[parity_block_id];
				prty_rack_id = get_rack_id(prty_node_id);

				if (copy_node_id == prty_node_id) {
					continue;
				}
				//printf("prty_node_id=%d\n",prty_node_id);
				// for the data node stored with the parity nodes in the same rack,
				// we directly send the data delta to the parity node

				// transfer the delta to node within same rack for data node
				if (rack_id == prty_rack_id) {

					tcd_dt[j].commit_app[k] = DIRECT_APPR;
					tcd_dt[j].updt_prty_id = prty_node_id;
					tcd_dt[j].updt_prty_nd_id[k] = prty_node_id;
					//tcd_dt[j].block_store_index = locate_store_index(prty_node_id, parity_block_id);
					memcpy(tcd_dt[j].next_dest[k], node_ip_set[prty_node_id], ip_len);
					//printf("dt_ip:%s + prty_ip:%s\n", tcd_dt[j].next_ip, tcd_dt[j].next_dest[k]);
				}

				// transfer the delta to node within same rack for replica node
				else if ((rack_id != prty_rack_id) && (copy_rack_id == prty_rack_id)) {

					tcd_copy[j].commit_app[k] = DIRECT_APPR;
					tcd_copy[j].updt_prty_id = prty_node_id;
					tcd_copy[j].updt_prty_nd_id[k] = prty_node_id;
					//tcd_copy[j].block_store_index = locate_store_index(prty_node_id, parity_block_id);
					memcpy(tcd_copy[j].next_dest[k], node_ip_set[prty_node_id], ip_len);
					//printf("copy_ip:%s + prty_ip:%s\n", tcd_copy[j].next_ip, tcd_copy[j].next_dest[k]);
					//printf("2\n");
				}

				// transfer the delta to node within different rack for replica node
				else if((prty_rack_id != rack_id) && (prty_rack_id != copy_rack_id)){
					tcd_copy[j].commit_app[k] = PARITY_DELTA_APPR;
					tcd_copy[j].updt_prty_id = prty_node_id;
					tcd_copy[j].updt_prty_nd_id[k] = prty_node_id;
					//tcd_copy[j].block_store_index = locate_store_index(prty_node_id, parity_block_id);
					memcpy(tcd_copy[j].next_dest[k], node_ip_set[prty_node_id], ip_len);
					//printf("3\n");
					//printf("copy_ip:%s + out_prty_ip:%s\n", tcd_copy[j].next_ip, tcd_copy[j].next_dest[k]);
				}

			}

			//free(prty_block_id);

			printf("count3=%d\n", count3);
			// send the commands to parity nodes
			for (h = 0; h < count3; h++) {		
				pthread_create(&send_cmd_prty_thread[count3], NULL, send_cmd_process, (void *)(tcd_prty + h));			
				//send_data(NULL, tcd_prty[h].next_ip, tcd_prty[h].port_num, NULL, tcd_prty[h], UPDT_DATA);
			}
			//printf("One\n");
			
			// send the cmd to the data node 
			pthread_create(&send_cmd_dt_thread[count1], NULL, send_cmd_process, (void *)(tcd_dt + j));
			
			// send the cmd to the copy node 
			pthread_create(&send_cmd_copy_thread[count1], NULL, send_cmd_process, (void *)(tcd_copy + j));
			count1++;
			
			//printf("Two\n");
			// wait acks from parity nodes
			para_recv_ack(updt_stripe_id, count3, CMMT_PORT);		

			//printf("Three\n");
			// wait the join of commands issued from data nodes involved in delta commit
			for (h = 0; h < count3; h++) {
				pthread_join(send_cmd_prty_thread[h], NULL);
			}
				

        }
   
        // wait the join of commands issued from data nodes involved in delta commit
        for(j=0; j<count1; j++)
            pthread_join(send_cmd_dt_thread[j], NULL);

        // wait the join of commands issued from copy nodes involved in delta commit
        for(j=0; j<count1; j++)
            pthread_join(send_cmd_copy_thread[j], NULL);


    }

    free(tcd_prty);
    free(tcd_dt);
    free(tcd_copy);
	free(prty_block_id);
	//free(updt_blck_num_racks);


}


/**
 * This function describes the processing for the request from the client
*/
void cru_md_process_req(UPDT_REQ_DATA* req){
    
    int local_block_id;
    int global_block_id;
    int node_id;
    int stripe_id;
    int block_id_in_stripe;
    int log_prty_id;
    int j;
 
	//printf("num_rcrd_strp = %d\n", num_rcrd_strp);
    // if the number of logged stripes exceeds a threshold
    // then launch parity update
    if(num_rcrd_strp >=max_updt_strps){
        
        cru_prty_updt(num_rcrd_strp);

        //re-init the mark_updt_stripes_table
        memset(mark_updt_stripes_tab, -1, sizeof(int)*(max_updt_strps+num_tlrt_strp)*(data_blocks+1));
        num_rcrd_strp=0;

    }
	
    // read the data from request  local_block_id(without parity blocks)
    local_block_id=req->local_block_id;
    stripe_id=local_block_id/data_blocks;
    block_id_in_stripe=local_block_id%data_blocks; 

    if(stripe_id>=stripe_num){
        printf("ERR: the stripe_id is larger than the maximun recorded stripe in block_map");
        exit(1);
    }

    // node info of that block    global_block_id(with parity blocks)
	global_block_id=stripe_id*num_blocks_in_stripe+block_id_in_stripe/(p-2)*p + block_id_in_stripe%(p-2);
    node_id=global_block_map[global_block_id];

	// check if the stripe is recorded
	for (j = 0; j < num_rcrd_strp; j++) {
		if (mark_updt_stripes_tab[j*(data_blocks + 1)] == stripe_id)
			break;
	}

	if (j >= num_rcrd_strp) {
		mark_updt_stripes_tab[j*(data_blocks + 1)] = stripe_id;
		num_rcrd_strp++;
	}

	// record the updated data chunks in the k-th stripe
	mark_updt_stripes_tab[j*(data_blocks + 1) + block_id_in_stripe + 1]++;

	
    // initialize the metadata info
    META_INFO* metadata=(META_INFO*)malloc(sizeof(META_INFO));

    metadata->data_block_id=local_block_id%data_blocks;
    metadata->stripe_id=stripe_id;
    metadata->port_num=UPDT_PORT;

    // fill the data node ip
    memcpy(metadata->next_ip, node_ip_set[node_id], ip_len);

    // fill the parity info
    // tell the data node where its corresponding parity node for data replication
    log_prty_id=prty_log_map[local_block_id];

    // select other its corresponding parity node in other rack to store replica
    // we current consider only one replica
    if(cru_num_rplc > 0){
        
        memset(metadata->updt_prty_nd_id, -1, sizeof(int)*r);
        metadata->updt_prty_nd_id[0]=log_prty_id;

    }

    // send the metadata back to the client
    send_req(NULL, client_ip, metadata->port_num, metadata, METADATA_INFO);

    free(metadata);

}


int main(int argc, char** argv){

    // read the data placement information
    read_block_map("block_map");

    // read the nodes for data replication in data logging
    cru_estbh_log_map();

    // read the storage information of data blocks
    get_block_store_order();

    // listen the request
    num_rcrd_strp=0;
    memset(mark_updt_stripes_tab, -1, sizeof(int)*(max_updt_strps+num_tlrt_strp)*(data_blocks+1));

    //initialize socket
    int connfd;
    int server_socket=init_server_socket(UPDT_PORT);
    int recv_len;
    int read_size;

    char* recv_buff=(char*)malloc(sizeof(UPDT_REQ_DATA));
    UPDT_REQ_DATA* req=(UPDT_REQ_DATA*)malloc(sizeof(UPDT_REQ_DATA));

    // initialize the sender info
    struct sockaddr_in sender_addr;
    socklen_t length=sizeof(sender_addr);
    
    if(listen(server_socket, 20) == -1){
        printf("Failed to listen.\n");
        exit(1);
    }
	// 1
    while(1){
		//printf("send req!\n");
        connfd=accept(server_socket, (struct sockaddr*)&sender_addr, &length);
        if(connfd<0){
            printf("connection fails\n");
            exit(1);
        }

        recv_len=0;
        read_size=0;
        while(recv_len<sizeof(UPDT_REQ_DATA)){

            read_size=read(connfd, recv_buff+recv_len, sizeof(UPDT_REQ_DATA)-recv_len);
            recv_len+=read_size;

        }
		//2
        // read the request and process it
        memcpy(req, recv_buff, sizeof(UPDT_REQ_DATA));
        cru_md_process_req(req);
		//3
        close(connfd);
        
    }
    
    free(recv_buff);
    free(req);

    return 0;
}








