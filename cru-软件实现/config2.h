#ifndef _CONFIG2_H
#define _CONFIG2_H

/* if the server to act as gateway switch is opened */
#define GTWY_OPEN             1

// ======================== Fill the erasure coding parameters ====================
/* erasure coding settings for X-code*/
#define p                     5
#define m                     2
#define r                     3
#define block_size            1024*1024 // suppose the chunk size is 1MB

// ======================== Fill the architecture parameters ====================

/* configurations of stripe and  data center */
#define data_blocks           30
#define data_nodes            10
#define parity_nodes          2 
#define total_nodes_num       12		//we set the number of chunks as the number of nodes in the local-cluster evaluation
#define max_block_per_rack    20
#define num_blocks_in_stripe  p*total_nodes_num
#define rack_num              4
#define node_num_per_rack     3 //we currently assume that each rack is composed of a constant number of nodes

// ============================= END ==============================================


/* varied configuration in evaluation*/
#define max_updt_strps        5 //it defines the maximum stripes to be updated before invoking delta commit

/*log table*/
#define max_log_block_cnt     1000
#define bucket_num            50
#define entry_per_bucket      max_log_block_cnt/bucket_num
#define max_num_store_blocks  1024*1024*10 //it denotes the maximum number of data chunks allowed to stored on a node
#define SERVER_PORT           2222

/* the number of kept replicas in CAU*/
//#define cru_num_rplc        (num_chunks_in_stripe-data_chunks)/2
#define cru_num_rplc          1

#define stripe_num            300000
#define string_len            100
#define w                     8
#define strlen                300

/* cluster settings */
#define ip_len                20
#define upper                 9999999

/* define update methods*/
#define CRU					  1

/* the operation type used in transmission  */
#define DATA_UPDT             1
#define DATA_PE               2 //it happens when the internal node commits the pse data to the parity node 
#define DATA_COMMIT           3
#define DELTA                 4 //it happens when the leaf node commits its data delta to the internal node
#define CMMT_CMLT             5 //it means that the commit is completed
#define DATA_LOG              6
#define UPDT_CMLT             7
#define LOG_CMLT              8
#define DATA_MVMNT            9
#define CMD_MVMNT             10
#define PRTY_UPDT             11
#define PRTY_UPDT_CMPLT       12
#define PRTY_LOG              13
#define TERMINATE             14
#define UPDT_REQ              15
#define MVMT_CMLT             16
#define INSIDE_LOG            17
#define OUTSIDE_LOG           18

//the role of each node in parity-delta approach
#define DATA_LEAF             1
#define DATA_INTERNAL         2
#define PARITY                3
#define IN_CHNK               4
#define OUT_CHNK              5

/* the roles of parity in data-delta-approach*/
#define PRTY_INTERNAL         6
#define PRTY_LEAF             7

/* the operations in PARIX*/
#define PARIX_UPDT            1
#define PARIX_CMMT            2
#define PARIX_OLD             3
#define PARIX_LGWT            4
#define PARIX_CMLT_CMMT       5
#define PARIX_UPDT_CMLT       6
#define PARIX_NEED_OLD_DT     7
#define PARIX_LOG_OLD_DT      8

/* commit approach */
#define DATA_DELTA_APPR       66
#define PARITY_DELTA_APPR     77
#define DIRECT_APPR           88

/*the port number*/ 
#define MAX_PORT_NUM          65535
#define MIN_PORT_NUM          1111
#define NO_DEFINED_PORT       -1

/*port num*/
#define UPDT_PORT             2222
#define GATEWAY_PORT		  2222
#define UPDT_ACK_PORT         6666
#define LOG_ACK_PORT          5555
#define PARIX_UPDT_PORT       3333
#define CMMT_PORT             5566
#define MVMT_PORT             6677

#define head_size             16
#define UPDT_DATA             1
#define ACK_INFO              2
#define CMD_INFO              3
#define REQ_INFO              4
#define METADATA_INFO         5


#define num_tlrt_strp 10 //it is to avoid the last write may cause the num_updt_stripes > max_updt_strps, so we have to consider this case

typedef struct _transmit_data{

    int send_size;
    int op_type; // DATA_UPDT for new data; DATA_COMMIT for partial encoding
    int stripe_id; //it will be used in commit
    int data_block_id;//new data or data_delta
    int updt_prty_id; //-1 in DATA_UPDT
    int num_recv_blks_itn; //it is used in the partial encoding to specify how many blocks to receive for a internal node
    int num_recv_blks_prt;
    int port_num;          //we specify port_num in commit
    int prty_delta_app_role; //the role is fixed for a given commit approach
    int data_delta_app_prty_role; //if a parity chunk is an internal node in a rack, then it will always be the internal node in the data-delta approach
    int block_store_index;
    int updt_prty_nd_id[r]; 
    int updt_prty_store_index[r];
    int commit_app[r];        //parity-delta-first or data-delta-first
    int parix_updt_data_id[data_blocks];                     //used to record the number of updated blocks in a stripe
    int log_type;
    char next_ip[ip_len];
    char sent_ip[ip_len]; //the ip addr to send the data 
    char from_ip[ip_len]; //record the ip addr of the sender 
    char next_dest[r][ip_len]; //it records the next dest ip addr to send the td->buff in the m parity chunks' renewals
    //for example, the next_addr of the leaf node in parity delta approach should the internal node 
    char buff[block_size];
}TRANSMIT_DATA;

typedef struct _aggt_send_data{

    int this_data_id;
    int this_rack_id;
    int this_stripe_id;
    int updt_prty_id;
    int data_delta_num;
    char next_ip[ip_len];
    int recv_delta_id[data_blocks]; 
    //int commit_app[num_chunks_in_stripe-data_chunks];        //parity-delta-first or data-delta-first
    char data_delta[block_size];

}AGGT_SEND_DATA;


typedef struct _ack_data{

    int send_size;
    int op_type;
    int stripe_id;
    int data_block_id;
    int updt_prty_id;
    int port_num;
    char next_ip[ip_len];

}ACK_DATA;


typedef struct _cmd_data{

    int send_size;
    int op_type; // DATA_UPDT for new data; DATA_COMMIT for partial encoding
    int stripe_id; //it will be used in commit
    int data_block_id;//new data or data_delta
    int updt_prty_id; //-1 in DATA_UPDT
    int num_recv_blks_itn; //it is used in the partial encoding to specify how many chunks to receive for a internal node
    int num_recv_blks_prt;
    int port_num;          //we specify port_num in commit
    int prty_delta_app_role; //the role is fixed for a given commit approach
    int data_delta_app_prty_role; //if a parity chunk is an internal node in a rack, then it will always be the internal node in the data-delta approach
    int block_store_index;
    int updt_prty_nd_id[r]; 
    int updt_prty_store_index[r];
    int commit_app[r];        //parity-delta-first or data-delta-first
    int parix_updt_data_id[data_blocks];                     //used to record the number of updated chunks in a stripe
    int log_type;
    char next_ip[ip_len];
    char sent_ip[ip_len]; //the ip addr to send the data 
    char from_ip[ip_len]; //record the ip addr of the sender 
    char next_dest[r][ip_len]; //it records the next dest ip addr to send the td->buff in the m parity chunks' renewals
    //for example, the next_addr of the leaf node in parity delta approach should the internal node 
}CMD_DATA;


typedef struct _updt_req_data{

    int op_type;
    int local_block_id;

}UPDT_REQ_DATA;


typedef struct _meta_info{

    int stripe_id;
    int data_block_id;
    int port_num;
    int block_store_index;
    int updt_prty_nd_id[r]; 
    int updt_prty_store_index[r];
    int if_first_update;
    char next_ip[ip_len];

}META_INFO;



typedef struct _recv_process_data{//it is used in receive data by multiple threads

    int connfd; 
    int recv_id;

}RECV_PROCESS_DATA;

typedef struct _recv_process_prty{//it is used in receive data by multiple threads

    int connfd; 
    int recv_id;
    int prty_delta_role;
    int prty_nd_id;

}RECV_PROCESS_PRTY;


int  global_block_map[stripe_num*num_blocks_in_stripe];
// int  local_block_map[stripe_num*data_blocks];
//int* encoding_matrix; //the encoding coefficient matrix
int  num_updt_strps;
int  newest_block_log_order[max_log_block_cnt*2];//it records the mapping between the newest version of chunk_id and the logged order
int  newest_copy_log_order[max_log_block_cnt*2];
//int  old_block_log_order[max_log_block_cnt*2]; //it records the old data that are logged for parity updates in parix
int  new_log_block_cnt; //it records the number of chunks logged
int  copy_log_block_cnt;
//int  old_log_block_cnt;
int  block_store_order[num_blocks_in_stripe*max_num_store_blocks]; //it records the data chunks stored in ascending order on a node in data_file
int  num_store_blocks[num_blocks_in_stripe];//it records the number of stored chunks, including data and parity chunks, on a node

int  intnl_recv_data_id[data_blocks];
char intnl_recv_data[block_size*data_blocks];

char in_block[block_size];
char out_block[block_size];

/* the global arrays used in parallel receiving acks */
int   updt_cmlt_count[r];
int   need_old_dt_count[r];
int   prty_log_cmplt_count[r];
int   prty_updt_count[r];
//int   mvmt_count[data_blocks];

/* global variables in CRU client */   
int mark_updt_stripes_tab[(max_updt_strps+num_tlrt_strp)*(data_blocks+1)]; //it records the updated data chunks and their stripes, the data_chunk column stores the data, while the data_chunk-th column store the stripe_id
int prty_log_map[stripe_num*data_blocks];
// mark the updated block
//int mark_updt_block_tab[num_blocks_in_stripe];



#endif
