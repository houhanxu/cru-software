#define _GNU_SOURCE2

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <net/if_arp.h>
#include <sys/time.h>

#include "common2.h"
#include "config2.h"

// block_id stored are sored in ascending orde
int if_commit_start;
int data_delta_role;
char pse_prty[data_blocks*block_size];
//int count1 = 0;
//int count2 = 0;


/**
 * This function writes a block to an append-only file
 * inside-log(the same rack) outside-log(outside the rack that contains the updated data block)
*/
void cru_log_write(TRANSMIT_DATA* td, int log_type) {

	int ret1, ret2;
	if (if_commit_start == 1) {
		//printf("UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU!\n");
		// when finishing the parity update, return to the default settings
		ret1 = truncate("cru_inside_log_file", 0);
		ret2 = truncate("cru_outside_log_file", 0);
		if (ret1 != 0) {
			printf("truncate inside_log fails\n");
			exit(1);
		}
		if (ret2 != 0) {
			printf("truncate outside_log fails\n");
			exit(1);
		}

		// re-check the file size
		struct stat stat_info;
		//struct stat stat_info2;
		stat("cru_inside_log_file", &stat_info);
		stat("cru_outside_log_file", &stat_info);

		new_log_block_cnt = 0;
		copy_log_block_cnt = 0;
		if_commit_start = 0;

	}


	// move fd at the bottom of file
	int local_block_id;
	int ret;
	local_block_id = td->stripe_id*data_blocks + td->data_block_id;

	// judge the log_type: inside-log or outside-log
	if (log_type == INSIDE_LOG) {
		// append the data and update the log records   inside-log
		log_write("cru_inside_log_file", td);
		ret = update_loged_blocks(local_block_id);
		//count1++;
		//printf("inside_log_count = %d\n", count1);
		if (ret != -1) {
			new_log_block_cnt++;
		}
		else {
			printf("fail to log!\n");
			exit(1);
		}
	}
	else if (log_type == OUTSIDE_LOG) {
		// append the data and update the log records   outside-log
		log_write("cru_outside_log_file", td);
		ret = update_loged_blocks2(local_block_id);
		//count2++;
		//printf("outsid_log_count = %d\n", count2);
		if (ret != -1) {
			copy_log_block_cnt++;
		}
		else {
			printf("fail to copy log!\n");
			exit(1);
		}
	}

	
     
}


/* In data delta logging phase
 * This function is performed at the data node(original rack), describing how a data node handles data update requests
*/
void cru_server_updt(TRANSMIT_DATA* td){
    
    //int ret1,ret2;
    int prty_node_id;
    
    char* old_data=(char*)malloc(sizeof(char)*block_size);
    char* data_delta=(char*)malloc(sizeof(char)*block_size);
    
    // read old data
    read_old_data(old_data, td->block_store_index);
	
    // calculate the delta and write it in an append-only file
    bitwiseXor(data_delta, td->buff, old_data, block_size);

    // replace the td->buff with delta
    memcpy(td->buff, data_delta, block_size);   

    // write the delta in an append-only
    cru_log_write(td,INSIDE_LOG);

    // if the number of replica to stored is one, then send the replica to on parity node in another rack
    if(cru_num_rplc==1){
        td->op_type=DATA_LOG;
        prty_node_id=td->updt_prty_nd_id[0];
        memcpy(td->sent_ip, node_ip_set[prty_node_id], ip_len);
        memcpy(td->next_ip, td->sent_ip, ip_len);
		//printf("send replica!\n");
        if(GTWY_OPEN)
            send_data(td, gateway_ip, td->port_num, NULL, NULL, UPDT_DATA);

        else
            send_data(td, td->sent_ip, td->port_num, NULL, NULL, UPDT_DATA);

        ACK_DATA* ack=(ACK_DATA*)malloc(sizeof(ACK_DATA));
        char* recv_buff=(char*)malloc(sizeof(ACK_DATA));
        // listen ack
        listen_ack(ack, recv_buff, td->stripe_id, td->data_block_id, td->updt_prty_id, LOG_ACK_PORT, LOG_CMLT);

        free(ack);
        free(recv_buff);
		free(old_data);
		free(data_delta);

    }

    
    // send ack to client if the update finishes
    send_ack(td->stripe_id, td->data_block_id, -1, client_ip, UPDT_ACK_PORT, LOG_CMLT);

}




///* In parity update phase
// * This function send the delta block to parity within different rack 
//*/
//void cru_delta_send_outside(TRANSMIT_DATA* td, int updt_prty_rack_id, int rack_id, int updt_prty_id, char* data_delta){
//
//    int temp_node_id;
//    int temp_rack_id;
//
//    // initiallize the structure
//    TRANSMIT_DATA* delta=(TRANSMIT_DATA*)malloc(sizeof(TRANSMIT_DATA));
//    delta->send_size=sizeof(TRANSMIT_DATA);
//    delta->op_type=DELTA;
//    delta->stripe_id=td->stripe_id;
//    delta->data_block_id=td->data_block_id;
//    delta->updt_prty_id=td->updt_prty_id;
//    delta->prty_delta_app_role=PARITY;
//    delta->port_num=td->port_num;
//
//    memcpy(delta->next_ip, td->next_dest[updt_prty_id], ip_len);
//    memcpy(delta->buff, data_delta, block_size);
//
//    temp_node_id=get_node_id(td->next_ip);
//    temp_rack_id=get_rack_id(temp_node_id);
//
//    if((GTWY_OPEN==1) && (temp_rack_id!=rack_id))
//        send_data(delta, gateway_ip, SERVER_PORT, NULL, NULL, UPDT_DATA);
//    
//    else        // SERVER_PORT not right
//        send_data(delta, td->next_dest[updt_prty_id], SERVER_PORT+updt_prty_id, NULL, NULL, UPDT_DATA);
//    
//    free(delta);
//
//    
//}

/**
 * This function send the parity delta block to parity node within the same rack
*/
void cru_direct_updt_action(TRANSMIT_DATA* td, int prty_index_id, char* data_delta){

    memcpy(td->buff, data_delta, block_size);
    send_data(td, td->next_dest[prty_index_id], SERVER_PORT+td->updt_prty_nd_id[prty_index_id], NULL, NULL, UPDT_DATA);
    
}


/**
 * This function is performed at the parity node to parallelly receive delta blocks
*/
void* prty_recv_data_process(void* ptr){

    RECV_PROCESS_PRTY rpp=*(RECV_PROCESS_PRTY *)ptr;

    //int i;
    int recv_len;
    int read_size;
    //int prty_rack_id;
    //int tmp_prty_rack_id;
    //int tmp_prty_node_id;
    //int count;

    char* recv_buff=(char*)malloc(sizeof(TRANSMIT_DATA));
    //char* pse_coded_data=(char*)malloc(sizeof(char)*block_size);

    // receive data
    recv_len=0;
    while(recv_len<sizeof(TRANSMIT_DATA)){

        read_size=read(rpp.connfd, recv_buff+recv_len, sizeof(TRANSMIT_DATA)-recv_len);
        recv_len+=read_size;
    }

	// copy the data
	TRANSMIT_DATA* td = (TRANSMIT_DATA *)malloc(sizeof(TRANSMIT_DATA));
	memcpy(td, recv_buff, sizeof(TRANSMIT_DATA));

	// if the commit approach is parity-delta commit or direct commit, then we directly collect the recieved parity deltas for further aggregation
	memcpy(pse_prty + rpp.recv_id*block_size, td->buff, block_size);

	free(td);
	free(recv_buff);
	return NULL;
}

/**
 * This function describes the functionality performed by the parity node in Parity Update
*/
void cru_prty_action(TRANSMIT_DATA* td, int rack_id, int server_socket){

    int i;
    int index;
    int global_block_id;
    int updt_prty_block_id;
    int stripe_id;
    char local_ip[ip_len];
    int prty_node_id;
    
    char* new_prty=(char*)malloc(sizeof(char)*block_size);

    stripe_id=td->stripe_id;
	updt_prty_block_id =td->data_block_id;		//��
    global_block_id=stripe_id*num_blocks_in_stripe+ updt_prty_block_id;
	data_delta_role = td->data_delta_app_prty_role;


    // get the node info and rack info
    memcpy(local_ip, td->sent_ip, ip_len);

    // find the parity node id
    for(i=0; i<total_nodes_num; i++){
        if(strcmp(node_ip_set[i], local_ip)==0)
            break;
    }

    prty_node_id=i;

    // read old parity
    read_old_data(new_prty, td->block_store_index);

    int* conn_fd=(int*)malloc(sizeof(int)*td->num_recv_blks_prt);
    pthread_t* pthread_mt=(pthread_t*)malloc(sizeof(pthread_t)*td->num_recv_blks_prt);
    memset(pthread_mt, 0, sizeof(pthread_t)*td->num_recv_blks_prt);

    // init the sender info
    struct sockaddr_in sender_addr;
    socklen_t length=sizeof(sender_addr);

    if(listen(server_socket,td->num_recv_blks_prt) == -1){
        printf("Failed to listen.\n");
        exit(1);
    }

    RECV_PROCESS_PRTY* rpp=(RECV_PROCESS_PRTY*)malloc(sizeof(RECV_PROCESS_PRTY)*td->num_recv_blks_prt);
    memset(rpp, 0, sizeof(RECV_PROCESS_PRTY)*td->num_recv_blks_prt);

    index=0;
    while(1){
        
        conn_fd[index]=accept(server_socket, (struct sockaddr*)&sender_addr, &length);

        rpp[index].connfd=conn_fd[index];
        rpp[index].recv_id=index;
		rpp[index].prty_delta_role = td->prty_delta_app_role;
        rpp[index].prty_nd_id=prty_node_id;

        // receive deltas in parallel
        pthread_create(&pthread_mt[index], NULL, prty_recv_data_process, (void *)(rpp+index));

        // if receive enough deltas, then break;
        index++;
        if(index>=td->num_recv_blks_prt)
            break;

    }

    for(i=0; i<index; i++){
        pthread_join(pthread_mt[i], NULL);
        close(conn_fd[i]);
    }

    // calculate the new parity. We first copy the first block to the new_prty
    // and then aggregate the remaining index-1 partial encoded blocks in pse_prty
    memcpy(new_prty, pse_prty, block_size);
	//printf("1111\n");
    //aggregate_data(new_prty, index-1, pse_prty+block_size);
	//printf("2222\n");
    // write the new parity
    flush_new_data(stripe_id, new_prty, global_block_id, td->block_store_index);

    // send ack to the metadata server
    send_ack(td->stripe_id, td->data_block_id, td->updt_prty_id, mt_svr_ip, CMMT_PORT, CMMT_CMLT);
    printf("Stripe-%d: Parity Node: Commit COmpletes in a Stripe!\n", td->stripe_id);

    free(new_prty);
    free(pthread_mt);
    free(rpp);
    free(conn_fd);

}



/*
 * This function is executed by each node, which performs the delta commit(Parity Update)
*/
void cru_server_commit(CMD_DATA* cmd){

    TRANSMIT_DATA* td=(TRANSMIT_DATA*)malloc(sizeof(TRANSMIT_DATA));

    memcpy(td, cmd, sizeof(CMD_DATA));
    td->send_size=sizeof(TRANSMIT_DATA);

    printf("Stripe-%d Update starts:\n", td->stripe_id);

    int local_block_id;
    int its_stripe_id;
    int node_id;
    int rack_id;
    int prty_cmmt;
    int prty_rack_id;
    int server_socket;

    server_socket=init_server_socket(SERVER_PORT+td->updt_prty_id);

    node_id=get_local_node_id();
    rack_id=get_rack_id(node_id);
    its_stripe_id=td->stripe_id;
    local_block_id=its_stripe_id*data_blocks+td->data_block_id;

    char* data_delta=(char*)malloc(sizeof(char)*block_size);

    // if it is a parity node, then perform the parity action
    if(td->prty_delta_app_role==PARITY){
		printf("PARITY NODE:\n");
        cru_prty_action(td, rack_id, server_socket);
		//cru_prty_updt(td, td->from_ip);
        close(server_socket);
		
        free(data_delta);
        free(td);
		if (if_commit_start == 0)
			if_commit_start = 1;

        return;
    }

	printf("DATA NODE:\n");
	
	td->op_type = DATA_COMMIT;

    // read the delta from log
    if(td->log_type==INSIDE_LOG)
        read_log_data(local_block_id, data_delta, "cru_inside_log_file");
    else if(td->log_type==OUTSIDE_LOG)
        read_log_data2(local_block_id, data_delta, "cru_outside_log_file");
    

    // if it is a data node or copy node, we consider the commit of the parity-delta
    for(prty_cmmt=0; prty_cmmt<r; prty_cmmt++){

		if (td->commit_app[prty_cmmt] == 0) 
			continue;	

        /***********************/
        td->port_num=SERVER_PORT+td->updt_prty_nd_id[prty_cmmt];   //
        //td->updt_prty_id= td->updt_prty_nd_id[prty_cmmt];
        prty_rack_id=get_rack_id(td->updt_prty_nd_id[prty_cmmt]);
        /**********************/

		memcpy(td->buff, data_delta, block_size);

        if((td->commit_app[prty_cmmt]==PARITY_DELTA_APPR) && (td->log_type==OUTSIDE_LOG)){
            printf("#OUT_RACK COMMIT APPR:\n");
			//memcpy(td->from_ip, td->next_ip, ip_len);
			// if the gateway server is established and the parity rack is a different rack, then send the delta to the gateway first
			// otherwise, send the delta to a parity node of that rack
			if ((GTWY_OPEN == 1) && (prty_rack_id != rack_id)) {

				memcpy(td->next_ip, td->next_dest[prty_cmmt], ip_len);
				send_data(td, gateway_ip, SERVER_PORT, NULL, NULL, UPDT_DATA);
				//printf("from %s to %s\n", td->from_ip, td->next_ip);
			}
			else
				send_data(td, td->next_dest[prty_cmmt], SERVER_PORT+td->updt_prty_nd_id[prty_cmmt], NULL, NULL, UPDT_DATA);
        }
        else if(td->commit_app[prty_cmmt]==DIRECT_APPR){

            printf("#DIRECT_APPR:\n");
			//memcpy(td->from_ip, td->next_ip, ip_len);
            memcpy(td->next_ip, td->next_dest[prty_cmmt], ip_len);
			//printf("from %s to %s\n",td->from_ip,td->next_ip);
			send_data(td, td->next_dest[prty_cmmt], SERVER_PORT+td->updt_prty_nd_id[prty_cmmt], NULL, NULL, UPDT_DATA);
			//printf("from %s to %s\n", td->from_ip, td->next_ip);
			
        }
    }

    // mark that the commit starts
    if(if_commit_start==0)
        if_commit_start=1;
    
    free(data_delta);
    free(td);
    close(server_socket);

}




int main(int argc, char** argv){

    int server_socket;
    int read_size;
    int recv_len;
    int connfd = 0;
    int ret1,ret2;
    int gateway_count;
    int send_size;
    int recv_data_type;
	int count_size;


    char local_ip[ip_len];
    char sender_ip[ip_len];

    if_commit_start=0;

    // truncate the log file in case of it still has the log info in last evaluation
    FILE* fd1;
	FILE* fd2;
    if((fd1=fopen("cru_inside_log_file", "r"))!=NULL){

        ret1=truncate("cru_inside_log_file", 0);
        if(ret1!=0){
            printf("truncate inside_log fails\n");
            exit(1);
        }
    }

	if ((fd2 = fopen("cru_outside_log_file", "r")) != NULL) {

		ret2 = truncate("cru_outside_log_file", 0);
		if (ret2 != 0) {
			printf("truncate outside_log fails\n");
			exit(1);
		}
	}

    // get local ip address
    GetLocalIp(local_ip);
    printf("local_ip=%s\n", local_ip);


    // initialize socket information
    server_socket=init_server_socket(UPDT_PORT);
    if(listen(server_socket,100) == -1){
        printf("Failed to listen.\n");
        exit(1);
    }

    // initialize the sender info
    struct sockaddr_in sender_addr;
    socklen_t length=sizeof(sender_addr);

    // initialize the hash_bucket
    new_log_block_cnt=0;
	copy_log_block_cnt=0;
    memset(newest_block_log_order, -1, sizeof(int)*max_log_block_cnt*2);
	memset(newest_copy_log_order, -1, sizeof(int)*max_log_block_cnt*2);

    num_updt_strps=0;

    // initialize the recv info
    TRANSMIT_DATA* td = (TRANSMIT_DATA*)malloc(sizeof(TRANSMIT_DATA));
    ACK_DATA* ack=(ACK_DATA*)malloc(sizeof(ACK_DATA));
    CMD_DATA* cmd=(CMD_DATA*)malloc(sizeof(CMD_DATA));

    char* recv_buff = (char*)malloc(sizeof(TRANSMIT_DATA));
    char* recv_head = (char*)malloc(head_size);

    gateway_count=0;
	count_size=0;

    while(1){
        connfd = accept(server_socket, (struct sockaddr*)&sender_addr, &length);

        memcpy(sender_ip, inet_ntoa(sender_addr.sin_addr), ip_len);
        memset(recv_head, '0', head_size);

        recv_len=0;
        //first read a part of data to determine the size of transmitted data
        read_size=read(connfd, recv_head, head_size);
        memcpy(&send_size, recv_head, sizeof(int));
        memcpy(recv_buff, recv_head, read_size);

        recv_len+=read_size;    

        while(recv_len < send_size){

            read_size=read(connfd, recv_buff+recv_len, send_size-recv_len);
            recv_len+=read_size;
        }

        recv_data_type=-1;

        // if it contains the updated data
        if(send_size==sizeof(TRANSMIT_DATA)){

            recv_data_type=UPDT_DATA;
            memcpy(td, recv_buff, sizeof(TRANSMIT_DATA));

        }

        // if it is an ack info
        else if(send_size==sizeof(ACK_DATA)){

            recv_data_type=ACK_INFO;
            memcpy(ack, recv_buff, sizeof(ACK_DATA));

        }

        // if it is a commit command
        else if(send_size==sizeof(CMD_DATA)){

            recv_data_type=CMD_INFO;
            memcpy(cmd, recv_buff, sizeof(CMD_DATA));
        }

        else{

            printf("ERR: unrecognized_send_size!\n");
            exit(1);

        }

        // if it is the gateway, then just forward the data to the destination node 
        if(((ret1=strcmp(gateway_local_ip, local_ip))==0) && (GTWY_OPEN==1)){

			if (recv_data_type == UPDT_DATA) {
				gateway_forward_updt_data(td, sender_ip);
				count_size++;
				printf("forwarded data count: %d \n", count_size);
			}
                

			else if (recv_data_type == ACK_INFO) 
				gateway_forward_ack_info(ack);
			

			else if (recv_data_type == CMD_INFO) 
				gateway_forward_cmd_data(cmd);
				

            gateway_count++;
            if(gateway_count%1000==0)
                printf("have forwarded %d requests (including update, ack, and cmd)\n", gateway_count);
			
            close(connfd);
            continue;

        }

		if (td->op_type == DATA_UPDT && recv_data_type == UPDT_DATA)
			cru_server_updt(td);   // this function will log

		else if (td->op_type == DATA_LOG && recv_data_type == UPDT_DATA) {//this is performed at the parity block side replica

			cru_log_write(td, OUTSIDE_LOG); // for replica

			//check if it receives data from gateway
			if ((strcmp(sender_ip, gateway_ip) == 0) && (GTWY_OPEN == 1))
				memcpy(sender_ip, td->from_ip, ip_len);

			send_ack(td->stripe_id, td->data_block_id, td->updt_prty_id, sender_ip, LOG_ACK_PORT, LOG_CMLT);

		}

        else if(cmd->op_type==DATA_COMMIT && recv_data_type==CMD_INFO)
            cru_server_commit(cmd);

        close(connfd);

    }

    free(td);
    free(recv_buff);
    free(ack);
    free(cmd);
    free(recv_head);
    close(server_socket);

    return 0;

}
